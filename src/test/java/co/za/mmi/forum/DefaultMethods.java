package co.za.mmi.forum;


import org.junit.Test;

/**
 * Example demonstrates inheritance and default methods as well as multiple inheritance.
 */
public class DefaultMethods {

	interface Vehicle {
		default void printRegistration(){
			System.out.println(this.getClass().getSimpleName() +  ": XER061GP");
		}
	}

	interface OtherVehicle {
		default void printRegistration(){
			System.out.println(this.getClass().getSimpleName() +  ": ABC669GP");
		}
	}

	class Car implements Vehicle {
	}

	class Truck implements OtherVehicle {
	}

	class SuperCar extends Car implements Vehicle, OtherVehicle {
		public void printRegistration(){
			OtherVehicle.super.printRegistration();
		}

	}

	@Test
	public void testCar(){
		Car car = new Car();
		car.printRegistration();

		Truck truck = new Truck();
		truck.printRegistration();

		SuperCar superCar = new SuperCar();
		superCar.printRegistration();
	}
}