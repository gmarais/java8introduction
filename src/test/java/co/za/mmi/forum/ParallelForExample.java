package co.za.mmi.forum;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Example demonstrate the use of a parallel loop and the performance comparison between the linear version.
 */
public class ParallelForExample {

	private static final int NUMBER_OF_ITERATIONS = 100_000;
	private static final boolean USE_COMPLEX_UNIT_OF_WORK = true;

	class UnitOfWork {

		private int g;
		private long total;

		public UnitOfWork(int g) {
			this.g = g;
		}

		public void run() {
			if (USE_COMPLEX_UNIT_OF_WORK) {
				doComplexWork();
			} else {
				doSimpleWork();
			}

			//	System.out.println(Thread.currentThread().getName() + " ,total calculated: " + total);
		}

		/**
		 * Simulated work creates a array of specified size, places a index in the element and then calculates the total
		 */
		private void doComplexWork() {
			int[] items = new int[g];
			for (int i = 0; i < g; i++) {
				items[i] = i;
			}
			total = 0;
			for (int i : items) {
				total += i;
			}
		}

		/**
		 * Simple work, just increment a number
		 */
		private void doSimpleWork() {
			total += g;
		}
	}

	@Test
	public void testRunLinear() {
		List<UnitOfWork> work = createWorkList();

		//priming run
		for (UnitOfWork complexUnitOfWork : work) {
			complexUnitOfWork.run();
		}

		//timed run
		long beforeTimeMillis = System.currentTimeMillis();
		work.forEach(UnitOfWork::run);

		System.out.println("Liner Total ms:" + (System.currentTimeMillis() - beforeTimeMillis));
	}

	@Test
	public void testRunParallel() {
		List<UnitOfWork> work = createWorkList();

		//priming run
		work.parallelStream().forEach(unitOfWork -> unitOfWork.run());

		//timed run
		long beforeTimeMillis = System.currentTimeMillis();
		work.parallelStream().forEach(UnitOfWork::run);
		System.out.println("Parallel Total ms:" + (System.currentTimeMillis() - beforeTimeMillis));
	}

	private List<UnitOfWork> createWorkList() {
		List<UnitOfWork> work = new ArrayList<>();
		for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
			work.add(new UnitOfWork(i));
		}
		return work;
	}
}