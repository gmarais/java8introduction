package co.za.mmi.forum.scratchpad;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MethodReferenceExample {

    @Test
    public void testMethodReference(){
        List<Long> longs = Arrays.asList(1L, 2L, 3L);
        longs.forEach(System.out::println);
    }
}
