package co.za.mmi.forum.scratchpad;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample {

    @Test
    public void testAboveStream(){
        List<String> names = Arrays.asList("Bob", "Max", "Tex", "John", "Sam");

        List<String> output = names.stream()
                .filter(s -> s.compareTo("M") >= 1)
                .collect(Collectors.toList());

        output.forEach(System.out::println);
    }
}
