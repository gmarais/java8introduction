package co.za.mmi.forum.scratchpad;

import org.junit.Test;

import java.util.Optional;

public class OptionalTest {

    @Test
    public void testOptional(){

        Optional<Integer> optionalEmpty = Optional.empty();
        Optional<Integer> optionalNull = Optional.ofNullable(null);
        Optional<Integer> optionalWithValue = Optional.of(66);

        System.out.println(optionalEmpty + ":" + optionalEmpty.isPresent());
        System.out.println(optionalNull + ":" + optionalNull.isPresent());
        System.out.println(optionalWithValue + ":" + optionalWithValue.isPresent());
    }

}