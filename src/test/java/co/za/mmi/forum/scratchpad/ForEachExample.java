package co.za.mmi.forum.scratchpad;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ForEachExample {

    @Test
    public void showForEachLoop(){

        List<String> values = Arrays.asList(new String[]{"One","Two", "Three"});

        //old foreach style loop
        for (String item : values) {
            System.out.println(item);
        }
        //single line closure based for each
        values.forEach(count -> System.out.println(count));

    }
}
