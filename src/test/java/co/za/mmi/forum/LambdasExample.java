package co.za.mmi.forum;

import org.junit.Test;

import java.util.*;

public class LambdasExample {
    @Test
    public void testLambdas() throws Exception {
        Thread oldWay = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Doing some old work");
            }
        });

        oldWay.run();

        Thread newWay = new Thread(() -> System.out.println("Doing some new work"));

        newWay.run();
    }

    @Test
    public void parameterizedLambda() throws Exception {
        ParameterizedLambdaExample example = (a, b) -> a + b;

        final int sum = example.sum(5, 7);

        System.out.println("Sum - " + sum);
    }

    @FunctionalInterface
    interface ParameterizedLambdaExample {
        int sum(int a, int b);
    }
}
