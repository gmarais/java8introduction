package co.za.mmi.forum;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Example of how to filter sets of data using streams
 */
public class StreamingObjects {

    private class Person{

        private final String name;
        private final City city;
        private final Integer age;

        public Person(String name, City city, Integer age) {
            this.name = name;
            this.city = city;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public City getCity() {
            return city;
        }

        public Integer getAge() {
            return age;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    private enum City{
        JOHANNESBURG,
        PRETORIA,
        CAPE_TOWN,
        DURBAN
    }

    @Test
    public void filterPeople(){
        Map<Integer, Person> people = new HashMap<>();

        people.put(1, new Person("Dutch", City.JOHANNESBURG, 22));
        people.put(2, new Person("Dillon", City.DURBAN, 18));
        people.put(3, new Person("Poncho", City.CAPE_TOWN, 22));
        people.put(4, new Person("Billy", City.CAPE_TOWN, 23));
        people.put(5, new Person("Mac", City.PRETORIA, 19));
        people.put(6, new Person("Anna", City.JOHANNESBURG, 25));

        EnumSet<City> cities = EnumSet.of(City.JOHANNESBURG, City.CAPE_TOWN, City.DURBAN);

        //get people by criteria
        people.values().stream()
                .filter(arg -> arg.getAge() > 20)
                .filter(arg -> cities.contains(arg.city))
                .filter(arg -> arg.getName().compareTo("M") < 1)
                .collect(Collectors.toList())
                .forEach(System.out::println);

        //get total age
        int sum = people.values().stream()
                .mapToInt(i -> i.getAge())
                .sum();

        System.out.println("Total age:" + sum);

        //get count of criteria
        long count = people.values().stream()
                .filter(arg -> City.JOHANNESBURG == arg.getCity())
                .count();

        System.out.println("Num people in JHB:" + count);
    }
}
