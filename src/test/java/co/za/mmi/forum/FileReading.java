package co.za.mmi.forum;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Example demonstrates simplified file reading and forEach
 */
public class FileReading {

	@Test
	public void readFile() throws IOException, URISyntaxException {
		URL resource = this.getClass().getClassLoader().getResource("sampleData.txt");

		List<String> lines = new ArrayList<>();

		try(Stream<String> input = Files.lines(Paths.get(resource.toURI()))){
			input.forEach(lines::add);
		}

		lines.forEach(System.out::println);
	}
}
