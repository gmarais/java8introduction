package co.za.mmi.forum;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Demonstrates the enhancements to the Collections API, for using with lambdas and method references
 */
public class CollectionsExample {

    @Test
    public void testSortWithJavaApi() throws Exception {
        List<Integer> numbers = Arrays.asList(1, 5, 9, 1, 3, 5, 7, 6, 4, 8, 9, 10, 8, 5, 1, 3, 6, 5, 4, 7, 1, 2, 3, 7, 8, 9);

        numbers.sort(Integer::compareTo);

        System.out.println(numbers);
    }

    @Test
    public void testSortWithCustomFunction() throws Exception {
        List<TestObject> objects = Arrays.asList(
                new TestObject(5, "words"),
                new TestObject(3, "stuff"),
                new TestObject(1, "blah")
        );

        objects.sort(TestObject::ourOwnCompare);

        System.out.println(objects);
    }

    @Test
    public void testReplaceAll() throws Exception {
        List<String> strings = new ArrayList<>();
        strings.add("ABBC");
        strings.add("DBEF");
        strings.add("GBHI");

        strings.replaceAll(s -> s.replaceFirst("B", ""));
        strings.forEach(System.out::println);
    }

    static class TestObject {
        int value;
        String name;

        public TestObject(int value, String name) {
            this.value = value;
            this.name = name;
        }

        static int ourOwnCompare(TestObject left, TestObject right) {
            return left.value - right.value;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
